package card;

public class TechnologyCard {
    private String name;
    private String description;
    private int cost;

    private TechnologyCard()  {
        // Objects of this class are not intended to be spawned without paramaters
    }

    public TechnologyCard(String name, String description, int cost) throws Exception {
        this.setName(name);
        this.setDescription(description);
        this.setCost(cost);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        if(name.isEmpty())
            throw new Exception("Empty name");
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) throws Exception {
        if(description.isEmpty())
            throw new Exception("Empty description");
        this.description = description;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) throws Exception {
        if(cost <= 0)
            throw new Exception("Invalid cost");
        this.cost = cost;
    }
}
