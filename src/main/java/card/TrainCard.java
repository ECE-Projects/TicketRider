package card;

public class TrainCard extends Card{
    private String color;

    private TrainCard() {
        // Objects of this class are not intended to be spawned without paramaters
    }

    public TrainCard(String color) throws Exception {
        this.setColor(color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) throws Exception {
        if(color.isEmpty())
            throw new Exception("Empty color");
        this.color = color;
    }
}
