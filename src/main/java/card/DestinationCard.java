package card;

public class DestinationCard extends Card {
    private String cityA;
    private String cityB;
    private int pointValue;

    private DestinationCard()  {
        // Objects of this class are not intended to be spawned without paramaters
    }

    public DestinationCard(String cityA, String cityB, int pointValue) throws Exception {
        this.setCityA(cityA);
        this.setCityB(cityB);
        this.setPointValue(pointValue);
    }

    public String getCityA() {
        return cityA;
    }

    public void setCityA(String cityA) throws Exception {
        if(cityA.isEmpty())
            throw new Exception("Empty city");
        this.cityA = cityA;
    }

    public String getCityB() {
        return cityB;
    }

    public void setCityB(String cityB) throws Exception {
        if(cityB.isEmpty())
            throw new Exception("Empty city");
        this.cityB = cityB;
    }

    public int getPointValue() {
        return pointValue;
    }

    public void setPointValue(int pointValue) throws Exception {
        if(pointValue <= 0)
            throw new Exception("Invalid pointValue");
        this.pointValue = pointValue;
    }
}
