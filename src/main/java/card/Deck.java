package card;

import java.util.ArrayList;
import java.util.Collections;


public class Deck {
    private ArrayList<Card> deck;
    private ArrayList<Card> discarded;

    public Deck() {
        // @Todo: Generate a deck
        this.shuffle();
    }

    public void shuffle() {
        Collections.shuffle(deck);
    }

    public void rebuildDeck() {
        deck.addAll(discarded);
        discarded.clear();
    }

    public void discard(ArrayList<Card> discard) throws Exception {
        if(!deck.containsAll(discard))
            throw new Exception("Card doesn't exist in the deck");
        deck.removeAll(discard);
        discarded.addAll(discard);
    }

    public int size() {
        return this.deck.size();
    }

    public ArrayList<Card> getDeck() {
        return deck;
    }

    public void setDeck(ArrayList<Card> deck) {
        this.deck = deck;
    }

    public ArrayList<Card> getDiscarded() {
        return discarded;
    }

    public void setDiscarded(ArrayList<Card> discarded) {
        this.discarded = discarded;
    }
}
