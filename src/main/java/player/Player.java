package player;

import card.Card;
import card.TechnologyCard;

import java.util.ArrayList;

public class Player {
    private int id;
    private int trainPiecesNumber;
    private int score;
    private String name;
    private String color;
    private ArrayList<TechnologyCard> technologyCards;
    private ArrayList<Card> heldDestinationCards;
    private ArrayList<Card> heldTrainCards;

    private Player() {
        // Objects of this class are not intended to be spawned without paramaters
    }

    public Player(
            int id,
            int trainPiecesNumber,
            int score,
            String name,
            String color,
            ArrayList<TechnologyCard> technologyCards,
            ArrayList<Card> heldDestinationCards,
            ArrayList<Card> heldTrainCards
    ) throws Exception {
        this.setId(id);
        this.setTrainPiecesNumber(trainPiecesNumber);
        this.setScore(score);
        this.setName(name);
        this.setColor(color);
        this.setTechnologyCards(technologyCards);
        this.setHeldDestinationCards(heldDestinationCards);
        this.setHeldTrainCards(heldTrainCards);
    }

    public int getId() throws Exception {
        if(id <= 0)
            throw new Exception("Invalid id");
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTrainPiecesNumber() {
        return trainPiecesNumber;
    }

    public void setTrainPiecesNumber(int trainPiecesNumber) throws Exception {
        if(trainPiecesNumber < 0)
            throw new Exception("Invalid trainPiecesNumber");
        this.trainPiecesNumber = trainPiecesNumber;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        if(name.isEmpty())
            throw new Exception("Empty name");
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) throws Exception {
        if(color.isEmpty())
            throw new Exception("Empty color");
        this.color = color;
    }

    public ArrayList<TechnologyCard> getTechnologyCards() {
        return technologyCards;
    }

    public void setTechnologyCards(ArrayList<TechnologyCard> technologyCards) {
        this.technologyCards = technologyCards;
    }

    public ArrayList<Card> getHeldDestinationCards() {
        return heldDestinationCards;
    }

    public void setHeldDestinationCards(ArrayList<Card> heldDestinationCards) {
        this.heldDestinationCards = heldDestinationCards;
    }

    public ArrayList<Card> getHeldTrainCards() {
        return heldTrainCards;
    }

    public void setHeldTrainCards(ArrayList<Card> heldTrainCards) {
        this.heldTrainCards = heldTrainCards;
    }
}
