package gameElement;

public class TrainPiece {
    private String pointA;
    private String pointB;
    private String colorA;
    private String colorB;
    private String colorC;
    private String colorD;

    private TrainPiece() {
        // Objects of this class are not intended to be spawned without parameters
    }

    public TrainPiece(
            String pointA,
            String pointB,
            String colorA,
            String colorB,
            String colorC,
            String colorD
    ) throws Exception {
        this.setPointA(pointA);
        this.setPointB(pointB);
        this.setColorA(colorA);
        this.setColorB(colorB);
        this.setColorC(colorC);
        this.setColorD(colorD);
    }

    public String getPointA() {
        return pointA;
    }

    public void setPointA(String pointA) throws Exception {
        if(pointA.isEmpty())
            throw new Exception("Empty pointA");
        this.pointA = pointA;
    }

    public String getPointB() {
        return pointB;
    }

    public void setPointB(String pointB) throws Exception {
        if(pointB.isEmpty())
            throw new Exception("Empty pointB");
        this.pointB = pointB;
    }

    public String getColorA() {
        return colorA;
    }

    public void setColorA(String colorA) throws Exception {
        if(colorA.isEmpty())
            throw new Exception("Empty colorA");
        this.colorA = colorA;
    }

    public String getColorB() {
        return colorB;
    }

    public void setColorB(String colorB) throws Exception {
        if(colorB.isEmpty())
            throw new Exception("Empty colorB");
        this.colorB = colorB;
    }

    public String getColorC() {
        return colorC;
    }

    public void setColorC(String colorC) throws Exception {
        if(colorC.isEmpty())
            throw new Exception("Empty colorC");
        this.colorC = colorC;
    }

    public String getColorD() {
        return colorD;
    }

    public void setColorD(String colorD) throws Exception {
        if(colorD.isEmpty())
            throw new Exception("Empty colorD");
        this.colorD = colorD;
    }
}
