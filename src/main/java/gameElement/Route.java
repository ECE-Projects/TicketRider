package gameElement;

public class Route {
    private int ownerId;
    private int trainRequirement;
    private String cityA;
    private String cityB;
    private String routeColor;

    protected Route() {
        // Objects of this class are not intended to be spawned without paramaters
    }

    public Route(
            int ownerId,
            int trainRequirement,
            String cityA,
            String cityB,
            String routeColor
    ) throws Exception {
        this.setOwnerId(ownerId);
        this.setTrainRequirement(trainRequirement);
        this.setCityA(cityA);
        this.setCityB(cityB);
        this.setRouteColor(routeColor);
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) throws Exception {
        if(ownerId <= 0)
            throw new Exception("Invalid ownerId");
        this.ownerId = this.ownerId;
    }

    public int getTrainRequirement() {
        return trainRequirement;
    }

    public void setTrainRequirement(int trainRequirement) throws Exception {
        if(trainRequirement < 0)
            throw new Exception("Invalid trainRequirement");
        this.trainRequirement = trainRequirement;
    }

    public String getCityA() {
        return cityA;
    }

    public void setCityA(String cityA) throws Exception {
        if(cityA.isEmpty())
            throw new Exception("Empty cityA");
        this.cityA = cityA;
    }

    public String getCityB() {
        return cityB;
    }

    public void setCityB(String cityB) throws Exception {
        if(cityB.isEmpty())
            throw new Exception("Empty cityB");
        this.cityB = cityB;
    }

    public String getRouteColor() {
        return routeColor;
    }

    public void setRouteColor(String routeColor) throws Exception {
        if(routeColor.isEmpty())
            throw new Exception("Empty routeColor");
        this.routeColor = routeColor;
    }
}
