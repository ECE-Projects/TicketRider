package gameElement;

public class FerryRoute extends Route {
    private int locomotiveRequirement;

    private FerryRoute() {
        // Objects of this class are not intended to be spawned without paramaters
    }

    public FerryRoute(
            int ownerId,
            int trainRequirement,
            int locomotiveRequirement,
            String cityA,
            String cityB,
            String routeColor
    ) throws Exception {
        super(ownerId, trainRequirement, cityA, cityB, routeColor);
        this.setLocomotiveRequirement(locomotiveRequirement);
    }

    public int getLocomotiveRequirement() {
        return locomotiveRequirement;
    }

    public void setLocomotiveRequirement(int locomotiveRequirement) throws Exception {
        if(locomotiveRequirement < 0)
            throw new Exception("Invalid locomotiveRequirement");
        this.locomotiveRequirement = locomotiveRequirement;
    }
}
