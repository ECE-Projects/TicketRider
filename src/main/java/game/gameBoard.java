package game;

import java.util.ArrayList;
import card.Card;
import gameElement.Route;
import player.Player;

public class gameBoard {
    private boolean lastTurn;
    private int currentPlayer;
    private ArrayList<Card> trainDeck;
    private ArrayList<Card> destinationDeck;
    private ArrayList<Player> players;
    private ArrayList<Route> routes;

    private gameBoard() {
        // Objects of this class are not intended to be spawned without
        // paramaters
    }

    public gameBoard(ArrayList<Card> trainDeck, ArrayList<Card>
            destinationDeck, ArrayList<Player> players) {
        this.trainDeck = trainDeck;
        this.destinationDeck = destinationDeck;
        this.players = players;
    }

    public void endTurn() {
        if(players.size() > currentPlayer + 1)
            currentPlayer++;
        else
            currentPlayer = 0;
    }

    public void buyRoute() {
        //@Todo
    }

    private void checkAllPlayersDestination() {
        for(int i = 0; i < players.size(); ++i)
            checkPlayerDestination(i);
    }

    private void checkPlayerDestination(int id) {
        //@Todo
    }

    public boolean isLastTurn() {
        return lastTurn;
    }

    public void setLastTurn(boolean lastTurn) {
        this.lastTurn = lastTurn;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayer) throws Exception {
        if(currentPlayer <= 0)
            throw new Exception("Invalid currentPlayer");
        this.currentPlayer = currentPlayer;
    }

    public ArrayList<Card> getTrainDeck() {
        return trainDeck;
    }

    public void setTrainDeck(ArrayList<Card> trainDeck) {
        this.trainDeck = trainDeck;
    }

    public ArrayList<Card> getDestinationDeck() {
        return destinationDeck;
    }

    public void setDestinationDeck(ArrayList<Card> destinationDeck) {
        this.destinationDeck = destinationDeck;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public ArrayList<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(ArrayList<Route> routes) {
        this.routes = routes;
    }
}
